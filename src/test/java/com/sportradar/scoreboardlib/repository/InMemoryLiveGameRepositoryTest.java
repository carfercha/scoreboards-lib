package com.sportradar.scoreboardlib.repository;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRCompetition;
import com.sportradar.scoreboardlib.service.LiveGameKeyGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InMemoryLiveGameRepositoryTest {

    private static final String HOME = "Home Team";

    private static final String AWAY = "Away Team";

    private static final BRCompetition COMPETITION = BRCompetition.FOOTBALL_WORLD_CUP;

    private static final String KEY = "Game-Key";

    @Mock
    private LiveGameKeyGenerator liveGameKeyGenerator;

    @InjectMocks
    private InMemoryLiveGameRepository inMemoryLiveGameRepository;

    private LiveGame liveGame;

    @BeforeEach
    public void setup() {
        liveGame = new LiveGame();
        liveGame.setHome(HOME);
        liveGame.setAway(AWAY);
        liveGame.setCompetition(COMPETITION.name());
    }

    @Test
    public void liveEventIsFoundAfterBeingSaved() {
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        inMemoryLiveGameRepository.save(liveGame);

        final Optional<LiveGame> existingLiveGame = inMemoryLiveGameRepository.find(KEY);

        assertThat(existingLiveGame.isPresent()).isTrue();
        assertThat(existingLiveGame.get()).isEqualTo(liveGame);
    }

    @Test
    public void liveEventIsNotFoundAfterBeingSavedAndDeleted() {
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        inMemoryLiveGameRepository.save(liveGame);
        inMemoryLiveGameRepository.remove(KEY);

        final Optional<LiveGame> existingLiveGame = inMemoryLiveGameRepository.find(KEY);

        assertThat(existingLiveGame.isPresent()).isFalse();
    }

}