package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.betradar.BRFixture;
import com.sportradar.scoreboardlib.domain.betradar.BRLiveScore;
import com.sportradar.scoreboardlib.domain.betradar.BRMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
public class DefaultMessageProcessorSelectorTest {

    @Mock
    private FixtureMessageProcessor fixtureMessageProcessor;

    @Mock
    private LiveScoreMessageProcessor liveScoreMessageProcessor;

    @InjectMocks
    private DefaultMessageProcessorSelector defaultMessageProcessorSelector;

    @Test
    public void fixtureMessageProcessorIsSelectedForFixtureMessage() {
        final BRMessage brMessage = new BRFixture();

        Optional<MessageProcessor> messageProcessor = defaultMessageProcessorSelector.select(brMessage);

        assertThat(messageProcessor.isPresent()).isTrue();
        assertThat(messageProcessor.get()).isEqualTo(fixtureMessageProcessor);
    }

    @Test
    public void liveScoreMessageProcessorIsSelectedForLiveScoreMessage() {
        final BRMessage brMessage = new BRLiveScore();

        Optional<MessageProcessor> messageProcessor = defaultMessageProcessorSelector.select(brMessage);

        assertThat(messageProcessor.isPresent()).isTrue();
        assertThat(messageProcessor.get()).isEqualTo(liveScoreMessageProcessor);
    }

    @Test
    public void noProcessorSelectedForNullBRMessage() {
        Optional<MessageProcessor> messageProcessor = defaultMessageProcessorSelector.select(null);

        assertThat(messageProcessor.isPresent()).isFalse();
    }

    @Test
    public void noProcessorSelectedForUnknownBrMessageType() {
        final BRMessage brMessage = new BRMessage() {
        };

        Optional<MessageProcessor> messageProcessor = defaultMessageProcessorSelector.select(brMessage);

        assertThat(messageProcessor.isPresent()).isFalse();
    }

}