package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRCompetition;
import com.sportradar.scoreboardlib.domain.betradar.BRLiveScore;
import com.sportradar.scoreboardlib.repository.LiveGameRepository;
import com.sportradar.scoreboardlib.service.LiveGameKeyGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LiveScoreMessageProcessorTest {

    private static final String HOME = "Home Team";

    private static final String AWAY = "Away Team";

    private static final String HOME_SCORE = "4";

    private static final String AWAY_SCORE = "1";

    private static final BRCompetition COMPETITION = BRCompetition.FOOTBALL_WORLD_CUP;

    private static final String KEY = "Game-Key";

    private static final Integer EXPECTED_HOME_SCORE = 4;

    private static final Integer EXPECTED_AWAY_SCORE = 1;

    private static final Integer EXPECTED_TOTAL_SCORE = 5;

    @Mock
    private LiveGameRepository liveGameRepository;

    @Mock
    private LiveGameKeyGenerator liveGameKeyGenerator;

    @InjectMocks
    private LiveScoreMessageProcessor liveScoreMessageProcessor;

    private BRLiveScore brLiveScore;

    private LiveGame liveGame;

    @BeforeEach
    public void setup() {
        brLiveScore = new BRLiveScore();
        brLiveScore.setHome(HOME);
        brLiveScore.setAway(AWAY);
        brLiveScore.setCompetition(COMPETITION);

        liveGame = new LiveGame();
        liveGame.setHome(HOME);
        liveGame.setAway(AWAY);
        liveGame.setCompetition(COMPETITION.name());
    }

    @Test
    public void doNotUpdateScoreWhenTheLiveGameDoesNotExist() {
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.empty());

        liveScoreMessageProcessor.process(brLiveScore);

        verify(liveGameRepository, never()).save(any());
    }

    @Test
    public void updateScoreWhenTheLiveGameExists() {
        brLiveScore.setHomeScore(HOME_SCORE);
        brLiveScore.setAwayScore(AWAY_SCORE);
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.of(liveGame));

        liveScoreMessageProcessor.process(brLiveScore);

        ArgumentCaptor<LiveGame> liveGameCaptor = ArgumentCaptor.forClass(LiveGame.class);
        verify(liveGameRepository).save(liveGameCaptor.capture());
        final LiveGame liveGame = liveGameCaptor.getValue();
        assertThat(liveGame.getHomeScore()).isEqualTo(EXPECTED_HOME_SCORE);
        assertThat(liveGame.getAwayScore()).isEqualTo(EXPECTED_AWAY_SCORE);
        assertThat(liveGame.getTotalScore()).isEqualTo(EXPECTED_TOTAL_SCORE);
        assertThat(liveGame.getHome()).isEqualTo(HOME);
        assertThat(liveGame.getAway()).isEqualTo(AWAY);
        assertThat(liveGame.getCompetition()).isEqualTo(COMPETITION.name());
    }

}