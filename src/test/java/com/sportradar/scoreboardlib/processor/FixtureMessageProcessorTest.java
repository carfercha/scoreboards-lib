package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRCompetition;
import com.sportradar.scoreboardlib.domain.betradar.BRFixture;
import com.sportradar.scoreboardlib.domain.betradar.BRFixtureStatus;
import com.sportradar.scoreboardlib.mapper.LiveGameMapper;
import com.sportradar.scoreboardlib.repository.LiveGameRepository;
import com.sportradar.scoreboardlib.service.LiveGameKeyGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FixtureMessageProcessorTest {

    private static final String HOME = "Home Team";

    private static final String AWAY = "Away Team";

    private static final BRCompetition COMPETITION = BRCompetition.FOOTBALL_WORLD_CUP;

    private static final String KEY = "Game-Key";

    @Mock
    private LiveGameRepository liveGameRepository;

    @Mock
    private LiveGameMapper liveGameMapper;

    @Mock
    private LiveGameKeyGenerator liveGameKeyGenerator;

    @InjectMocks
    private FixtureMessageProcessor fixtureMessageProcessor;

    private BRFixture brFixture;

    private LiveGame liveGame;

    @BeforeEach
    public void setup() {
        brFixture = new BRFixture();
        brFixture.setHome(HOME);
        brFixture.setAway(AWAY);
        brFixture.setCompetition(COMPETITION);

        liveGame = new LiveGame();
    }

    @Test
    public void liveGameIsCreatedWhenInRunningFixtureIsReceivedForFirstTime() {
        brFixture.setBrFixtureStatus(BRFixtureStatus.IN_PROGRESS);
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.empty());
        when(liveGameMapper.map(brFixture)).thenReturn(liveGame);

        fixtureMessageProcessor.process(brFixture);

        verify(liveGameRepository).save(liveGame);
    }

    @Test
    public void liveGameNotCreatedWhenInRunningFixtureIsReceivedOtherThanFirstTime() {
        brFixture.setBrFixtureStatus(BRFixtureStatus.IN_PROGRESS);
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.of(liveGame));

        fixtureMessageProcessor.process(brFixture);
        
        verify(liveGameRepository, never()).save(liveGame);
        verify(liveGameRepository, never()).remove(KEY);
    }

    @Test
    public void liveGameIsNotCreatedWhenEndedFixtureIsReceived() {
        brFixture.setBrFixtureStatus(BRFixtureStatus.ENDED);
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.empty());

        fixtureMessageProcessor.process(brFixture);

        verify(liveGameRepository, never()).save(liveGame);
        verify(liveGameRepository, never()).remove(KEY);
    }

    @Test
    public void liveGameIsRemovedWhenEndedFixtureIsReceivedAndGameExists() {
        brFixture.setBrFixtureStatus(BRFixtureStatus.ENDED);
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.of(liveGame));

        fixtureMessageProcessor.process(brFixture);

        verify(liveGameRepository, never()).save(liveGame);
        verify(liveGameRepository).remove(KEY);
    }

    @Test
    public void doNothingWhenLiveEventExistsAndInRunningFixtureReceived() {
        brFixture.setBrFixtureStatus(BRFixtureStatus.IN_PROGRESS);
        when(liveGameKeyGenerator.generate(anyString(), anyString(), anyString())).thenReturn(KEY);
        when(liveGameRepository.find(KEY)).thenReturn(Optional.of(liveGame));

        fixtureMessageProcessor.process(brFixture);

        verify(liveGameRepository, never()).save(liveGame);
        verify(liveGameRepository, never()).remove(KEY);
    }

    @Test
    public void doNothingWhenCompetitionIsOtherThanFootballWC() {
        brFixture.setCompetition(BRCompetition.UNKNOWN);
        brFixture.setBrFixtureStatus(BRFixtureStatus.IN_PROGRESS);

        fixtureMessageProcessor.process(brFixture);

        verify(liveGameRepository, never()).save(liveGame);
        verify(liveGameRepository, never()).remove(KEY);
    }


}