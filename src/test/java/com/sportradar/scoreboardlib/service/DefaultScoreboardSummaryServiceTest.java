package com.sportradar.scoreboardlib.service;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.repository.LiveGameRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultScoreboardSummaryServiceTest {

    @Mock
    private LiveGameRepository liveGameRepository;

    @InjectMocks
    private DefaultScoreboardSummaryService defaultScoreboardSummaryService;

    @Test
    public void liveGamesAreSortedByTotals() {
        final LiveGame liveGame1 = LiveGame.builder()
                .home("Home1").away("Away1").homeScore(4).awayScore(3).totalScore(7)
                .creation(LocalDateTime.of(2021, 1, 15, 10, 0))
                .build();
        final LiveGame liveGame2 = LiveGame.builder()
                .home("Home2").away("Away2").homeScore(1).awayScore(1).totalScore(2)
                .creation(LocalDateTime.of(2021, 1, 15, 10, 0))
                .build();
        final LiveGame liveGame3 = LiveGame.builder()
                .home("Home3").away("Away3").homeScore(0).awayScore(5).totalScore(5)
                .creation(LocalDateTime.of(2021, 1, 10, 10, 0))
                .build();

        when(liveGameRepository.findAll()).thenReturn(Arrays.asList(liveGame1, liveGame2, liveGame3));

        List<LiveGame> liveGames = defaultScoreboardSummaryService.getSummary();

        assertThat(liveGames.size()).isEqualTo(3);
        assertThat(liveGames.get(0)).isEqualTo(liveGame1);
        assertThat(liveGames.get(1)).isEqualTo(liveGame3);
        assertThat(liveGames.get(2)).isEqualTo(liveGame2);
    }

    @Test
    public void liveGamesAreSortedByCreationDateWhenTotalAreTheSame() {
        final LiveGame liveGame1 = LiveGame.builder()
                .home("Home1").away("Away1").homeScore(4).awayScore(3).totalScore(7)
                .creation(LocalDateTime.of(2021, 1, 12, 10, 0))
                .build();
        final LiveGame liveGame2 = LiveGame.builder()
                .home("Home2").away("Away2").homeScore(1).awayScore(6).totalScore(7)
                .creation(LocalDateTime.of(2021, 1, 17, 10, 0))
                .build();
        final LiveGame liveGame3 = LiveGame.builder()
                .home("Home3").away("Away3").homeScore(2).awayScore(5).totalScore(7)
                .creation(LocalDateTime.of(2021, 1, 10, 10, 0))
                .build();

        when(liveGameRepository.findAll()).thenReturn(Arrays.asList(liveGame1, liveGame2, liveGame3));

        List<LiveGame> liveGames = defaultScoreboardSummaryService.getSummary();

        assertThat(liveGames.size()).isEqualTo(3);
        assertThat(liveGames.get(0)).isEqualTo(liveGame2);
        assertThat(liveGames.get(1)).isEqualTo(liveGame1);
        assertThat(liveGames.get(2)).isEqualTo(liveGame3);
    }
}