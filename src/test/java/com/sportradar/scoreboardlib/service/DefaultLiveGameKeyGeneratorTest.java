package com.sportradar.scoreboardlib.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class DefaultLiveGameKeyGeneratorTest {

    private static final String COMPETITION = "COMP";

    private static final String HOME = "HOME";

    private static final String AWAY = "AWAY";

    private static final String EXPECTED = "COMP-HOME-AWAY";

    private final DefaultLiveGameKeyGenerator defaultLiveGameKeyGenerator = new DefaultLiveGameKeyGenerator();

    @Test
    public void keyIsGenerated() {
        assertThat(defaultLiveGameKeyGenerator.generate(COMPETITION, HOME, AWAY)).isEqualTo(EXPECTED);
    }

}