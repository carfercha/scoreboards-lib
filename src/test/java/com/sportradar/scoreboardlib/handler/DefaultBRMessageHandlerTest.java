package com.sportradar.scoreboardlib.handler;

import com.sportradar.scoreboardlib.domain.betradar.BRFixture;
import com.sportradar.scoreboardlib.domain.betradar.BRMessage;
import com.sportradar.scoreboardlib.processor.MessageProcessor;
import com.sportradar.scoreboardlib.processor.MessageProcessorSelector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DefaultBRMessageHandlerTest {

    @Mock
    private MessageProcessorSelector messageProcessorSelector;

    @InjectMocks
    private DefaultBRMessageHandler defaultBRMessageHandler;

    @Mock
    private MessageProcessor messageProcessor;

    @Test
    public void brMessagesIsHandled() {
        final BRMessage brMessage = new BRFixture();
        when(messageProcessorSelector.select(brMessage)).thenReturn(Optional.of(messageProcessor));

        defaultBRMessageHandler.handle(brMessage);

        verify(messageProcessorSelector).select(brMessage);
        verify(messageProcessor).process(brMessage);
    }

    @Test
    public void brMessageNotHandledWhenNullSelector() {
        final BRMessage brMessage = new BRFixture();
        when(messageProcessorSelector.select(brMessage)).thenReturn(Optional.empty());

        defaultBRMessageHandler.handle(brMessage);

        verify(messageProcessorSelector).select(brMessage);
        verifyNoInteractions(messageProcessor);
    }

}