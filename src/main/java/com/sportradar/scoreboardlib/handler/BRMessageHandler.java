package com.sportradar.scoreboardlib.handler;

import com.sportradar.scoreboardlib.domain.betradar.BRMessage;

/**
 * Handles a BetRadar Message.
 */
public interface BRMessageHandler {
    void handle(final BRMessage brMessage);
}
