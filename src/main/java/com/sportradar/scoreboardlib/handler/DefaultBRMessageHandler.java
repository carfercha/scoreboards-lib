package com.sportradar.scoreboardlib.handler;

import com.sportradar.scoreboardlib.domain.betradar.BRMessage;
import com.sportradar.scoreboardlib.processor.MessageProcessor;
import com.sportradar.scoreboardlib.processor.MessageProcessorSelector;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class DefaultBRMessageHandler implements BRMessageHandler {

    private final MessageProcessorSelector messageProcessorSelector;

    @Override
    public void handle(final BRMessage brMessage) {
        final Optional<MessageProcessor> messageProcessor = messageProcessorSelector.select(brMessage);
        messageProcessor.ifPresent(processor -> processor.process(brMessage));
    }
}
