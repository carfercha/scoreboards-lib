package com.sportradar.scoreboardlib.service;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.repository.LiveGameRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link ScoreboardSummaryService}.
 */
@AllArgsConstructor
@Component
public class DefaultScoreboardSummaryService implements ScoreboardSummaryService {

    private final LiveGameRepository liveGameRepository;

    @Override
    public List<LiveGame> getSummary() {
        return liveGameRepository.findAll().stream()
                .sorted(Comparator.comparing(LiveGame::getTotalScore, Comparator.reverseOrder())
                        .thenComparing(LiveGame::getCreation, Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }
}
