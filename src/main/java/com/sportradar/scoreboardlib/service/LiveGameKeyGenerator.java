package com.sportradar.scoreboardlib.service;

import com.sportradar.scoreboardlib.domain.LiveGame;

/**
 * Generates a key for a {@link LiveGame}.
 */
public interface LiveGameKeyGenerator {

    String generate(String competition, String home, String away);
}
