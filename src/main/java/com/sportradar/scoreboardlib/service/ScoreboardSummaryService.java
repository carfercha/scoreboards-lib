package com.sportradar.scoreboardlib.service;

import com.sportradar.scoreboardlib.domain.LiveGame;

import java.util.List;

/**
 * Service interface for Scorboard Summary.
 */
public interface ScoreboardSummaryService {

    List<LiveGame> getSummary();

}
