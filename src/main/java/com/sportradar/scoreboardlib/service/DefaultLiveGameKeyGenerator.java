package com.sportradar.scoreboardlib.service;

import org.springframework.stereotype.Component;

@Component
public class DefaultLiveGameKeyGenerator implements LiveGameKeyGenerator {

    private static final String KEY_FORMAT = "%s-%s-%s";

    @Override
    public String generate(String competition, String home, String away) {
        return String.format(KEY_FORMAT, competition, home, away);
    }
}
