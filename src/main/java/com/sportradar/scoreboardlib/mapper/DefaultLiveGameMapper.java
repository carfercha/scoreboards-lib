package com.sportradar.scoreboardlib.mapper;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRFixture;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * Maps a {@link BRFixture} into a {@link LiveGame}.
 */
@Component
public class DefaultLiveGameMapper implements LiveGameMapper {

    private static final Integer INIT_SCORE = 0;

    @Override
    public LiveGame map(BRFixture brFixture) {
        return LiveGame.builder()
                .home(brFixture.getHome())
                .away(brFixture.getAway())
                .competition(brFixture.getCompetition().name())
                .creation(LocalDateTime.now())
                .homeScore(INIT_SCORE)
                .awayScore(INIT_SCORE)
                .totalScore(INIT_SCORE)
                .build();
    }
}
