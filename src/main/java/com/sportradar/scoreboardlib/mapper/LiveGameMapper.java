package com.sportradar.scoreboardlib.mapper;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRFixture;

/**
 * Mapper for {@link LiveGame}.
 */
public interface LiveGameMapper {

    LiveGame map(final BRFixture brFixture);
}
