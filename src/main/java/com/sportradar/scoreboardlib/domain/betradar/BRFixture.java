package com.sportradar.scoreboardlib.domain.betradar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Fixture information sent by BetRadar.
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BRFixture extends BRDescribableFixture {

    private BRFixtureStatus brFixtureStatus;

}
