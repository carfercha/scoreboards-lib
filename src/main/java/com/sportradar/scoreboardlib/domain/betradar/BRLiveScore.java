package com.sportradar.scoreboardlib.domain.betradar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Live Score information sent by BetRadar.
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BRLiveScore extends BRDescribableFixture {

    private String homeScore;

    private String awayScore;

}
