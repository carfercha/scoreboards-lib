package com.sportradar.scoreboardlib.domain.betradar;

import lombok.Data;

/**
 * Abstract class that contains basic information to describe a Fixture.
 */
@Data
public abstract class BRDescribableFixture implements BRMessage {

    private String home;

    private String away;

    private BRCompetition competition;

}
