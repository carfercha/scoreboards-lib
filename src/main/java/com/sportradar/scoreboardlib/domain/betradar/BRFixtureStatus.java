package com.sportradar.scoreboardlib.domain.betradar;

/**
 * Enum that represent all possible fixture status from BetRadar.
 */
public enum BRFixtureStatus {

    IN_PROGRESS,
    ENDED

}
