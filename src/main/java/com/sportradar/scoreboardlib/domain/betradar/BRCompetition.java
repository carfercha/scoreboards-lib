package com.sportradar.scoreboardlib.domain.betradar;

/**
 * Enum that represent possible Competition values coming from BetRadar.
 */
public enum BRCompetition {
    FOOTBALL_WORLD_CUP,
    UNKNOWN
}
