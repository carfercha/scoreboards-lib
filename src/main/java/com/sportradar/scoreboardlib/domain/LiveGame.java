package com.sportradar.scoreboardlib.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Represents a Live Game Event.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class LiveGame {

    private String home;

    private String away;

    private String competition;

    private LocalDateTime creation;

    private Integer homeScore;

    private Integer awayScore;

    private Integer totalScore;

}
