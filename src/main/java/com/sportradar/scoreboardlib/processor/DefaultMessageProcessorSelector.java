package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.betradar.BRFixture;
import com.sportradar.scoreboardlib.domain.betradar.BRLiveScore;
import com.sportradar.scoreboardlib.domain.betradar.BRMessage;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class DefaultMessageProcessorSelector implements MessageProcessorSelector {

    private final FixtureMessageProcessor fixtureMessageProcessor;

    private final LiveScoreMessageProcessor liveScoreMessageProcessor;

    @Override
    public Optional<MessageProcessor> select(BRMessage brMessage) {
        if (brMessage instanceof BRFixture) {
            return Optional.of(fixtureMessageProcessor);
        }
        if (brMessage instanceof BRLiveScore) {
            return Optional.of(liveScoreMessageProcessor);
        }
        return Optional.empty();
    }
}
