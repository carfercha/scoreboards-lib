package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.betradar.BRMessage;

import java.util.Optional;

/**
 * Selects a {@link MessageProcessor} based on {@link BRMessage}.
 */
public interface MessageProcessorSelector {

    Optional<MessageProcessor> select(final BRMessage brMessage);

}
