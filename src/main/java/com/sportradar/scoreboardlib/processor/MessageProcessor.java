package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.betradar.BRMessage;

/**
 * Process a {@link BRMessage}.
 */
public interface MessageProcessor {
    void process(final BRMessage brMessage);
}
