package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRLiveScore;
import com.sportradar.scoreboardlib.domain.betradar.BRMessage;
import com.sportradar.scoreboardlib.repository.LiveGameRepository;
import com.sportradar.scoreboardlib.service.LiveGameKeyGenerator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * MessageProcessor implementation for {@link BRLiveScore}.
 */
@AllArgsConstructor
@Component
public class LiveScoreMessageProcessor implements MessageProcessor {

    private final LiveGameRepository liveGameRepository;

    private final LiveGameKeyGenerator liveGameKeyGenerator;

    @Override
    public void process(BRMessage brMessage) {
        final BRLiveScore brLiveScore = (BRLiveScore) brMessage;
        final String key = liveGameKeyGenerator.generate(
                brLiveScore.getCompetition().name(), brLiveScore.getHome(), brLiveScore.getAway());
        final Optional<LiveGame> existingLiveGame = liveGameRepository.find(key);

        existingLiveGame.ifPresent(liveGame -> {
            updateScores(liveGame, brLiveScore);
            liveGameRepository.save(liveGame);
        });
    }

    private void updateScores(final LiveGame liveGame, final BRLiveScore brLiveScore) {
        final Integer homeScore = Integer.parseInt(brLiveScore.getHomeScore());
        final Integer awayScore = Integer.parseInt(brLiveScore.getAwayScore());
        liveGame.setHomeScore(homeScore);
        liveGame.setAwayScore(awayScore);
        liveGame.setTotalScore(homeScore + awayScore);
    }
}
