package com.sportradar.scoreboardlib.processor;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.domain.betradar.BRCompetition;
import com.sportradar.scoreboardlib.domain.betradar.BRFixture;
import com.sportradar.scoreboardlib.domain.betradar.BRFixtureStatus;
import com.sportradar.scoreboardlib.domain.betradar.BRMessage;
import com.sportradar.scoreboardlib.mapper.LiveGameMapper;
import com.sportradar.scoreboardlib.repository.LiveGameRepository;
import com.sportradar.scoreboardlib.service.LiveGameKeyGenerator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * MessageProcessor implementation for {@link BRFixture}.
 */
@Slf4j
@AllArgsConstructor
@Component
public class FixtureMessageProcessor implements MessageProcessor {

    private final LiveGameRepository liveGameRepository;

    private final LiveGameMapper liveGameMapper;

    private final LiveGameKeyGenerator liveGameKeyGenerator;

    @Override
    public void process(final BRMessage brMessage) {
        final BRFixture brFixture = (BRFixture) brMessage;
        if (!allowedCompetition(brFixture)) {
            log.info("Competition not being traded: " + brFixture.getCompetition().name());
            return;
        }
        final String key = liveGameKeyGenerator.generate(
                brFixture.getCompetition().name(), brFixture.getHome(), brFixture.getAway());
        final Optional<LiveGame> existingLiveGame = liveGameRepository.find(key);

        if (!existingLiveGame.isPresent() && inProgress(brFixture)) {
            liveGameRepository.save(liveGameMapper.map(brFixture));
        } else if (existingLiveGame.isPresent() && ended(brFixture)) {
            liveGameRepository.remove(key);
        }
    }

    private boolean allowedCompetition(BRFixture brFixture) {
        return BRCompetition.FOOTBALL_WORLD_CUP == brFixture.getCompetition();
    }

    private boolean ended(final BRFixture brFixture) {
        return BRFixtureStatus.ENDED == brFixture.getBrFixtureStatus();
    }

    private boolean inProgress(final BRFixture brFixture) {
        return BRFixtureStatus.IN_PROGRESS == brFixture.getBrFixtureStatus();
    }
}
