package com.sportradar.scoreboardlib.repository;

import com.sportradar.scoreboardlib.domain.LiveGame;
import com.sportradar.scoreboardlib.service.LiveGameKeyGenerator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In-Memory implementation of {@link LiveGameRepository}.
 */
@AllArgsConstructor
@Component
public class InMemoryLiveGameRepository implements LiveGameRepository {

    private final LiveGameKeyGenerator liveGameKeyGenerator;

    private static final Map<String, LiveGame> liveGames = new ConcurrentHashMap<>();

    @Override
    public Optional<LiveGame> find(String id) {
        return Optional.ofNullable(liveGames.get(id));
    }

    @Override
    public void save(LiveGame liveGame) {
        final String key = liveGameKeyGenerator.generate(
                liveGame.getCompetition(), liveGame.getHome(), liveGame.getAway());
        liveGames.put(key, liveGame);
    }

    @Override
    public void remove(String id) {
        liveGames.remove(id);
    }

    @Override
    public List<LiveGame> findAll() {
        return new ArrayList<>(liveGames.values());
    }
}
