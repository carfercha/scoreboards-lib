package com.sportradar.scoreboardlib.repository;

import com.sportradar.scoreboardlib.domain.LiveGame;

import java.util.List;
import java.util.Optional;

/**
 * Repository Interface for {@link LiveGame}.
 */
public interface LiveGameRepository {

    Optional<LiveGame> find(String id);

    void save(final LiveGame liveGame);

    void remove(String id);

    List<LiveGame> findAll();

}
