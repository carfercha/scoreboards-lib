# Live ScoreBoards Library

# Assumptions
* Our partner is called BetRadar
* Our partner will NOT send delta messages for the scores, 
  instead we will receive the current score in each LiveScore message
* Our partner's model is defined under package com.sportradar.scoreboardlib.domain.betradar  

# How to integrate
* Consider BRMessageHandler as an entry point.
* Use ScoreboardSummaryService to get the summary.
